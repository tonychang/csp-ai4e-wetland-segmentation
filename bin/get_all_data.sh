#!/bin/bash
DOWNLOAD_SCRIPT=./bin/download_state_data.sh
STATES=$(cat ./bin/states.csv | cut -d, -f2)

for state in $STATES; do
  $DOWNLOAD_SCRIPT $state
done
