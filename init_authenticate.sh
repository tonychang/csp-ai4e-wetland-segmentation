#!/bin/bash
dockerhub_image="ericstofferahn/dacqre:public"

docker run -it --rm -v $(pwd):/content -w /content $dockerhub_image ./authentication/authenticate_once.bash
